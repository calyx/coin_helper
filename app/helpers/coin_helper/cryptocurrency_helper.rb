module CoinHelper
  module CryptocurrencyHelper

    def coin_data(coin)
      CoinHelper::DATA[coin.upcase] || {
        "name" => {
          "en" => coin.upcase
        },
        "icon" => "generic",
        "rank" => 1000,
        "carbon" => "F",
        "stable" => false,
        "private" => false
      }
    end

    #
    # returns coin symbols, sorted by rank, that are defined and in the symbols array
    #
    def coins_by_rank(symbols, limit:0)
      coins = symbols.map {|s| s.upcase }.select {|s| coin_definition_exists?(s) }
      coins.sort! {|a,b|
        coin_data(a)["rank"] <=> coin_data(b)["rank"]
      }
      if limit == 0
        return coins
      else
        return coins[0..(limit-1)]
      end
    end

    def private_coins(symbols=[])
      coin_intersection(CoinHelper::PRIVATE, symbols)
    end

    def green_coins(symbols=[])
      coin_intersection(CoinHelper::CARBON_A, symbols)
    end

    def greenish_coins(symbols=[])
      coin_intersection(CoinHelper::CARBON_B, symbols)
    end

    def stable_coins(symbols=[])
      coin_intersection(CoinHelper::STABLE, symbols)
    end

    def coin_definition_exists?(coin)
      !!CoinHelper::DATA[coin.upcase]
    end

    def coin_name(coin)
      record = coin_data(coin)
      record["name"][I18n.locale] || record["name"]["en"]
    end

    def coin_img(coin, size:"22", color: "color")
      record = coin_data(coin)
      name = record["icon"] || "unknown"
      path = asset_path(File.join("coin_helper", color, size, "#{name}.png"))
      content_tag "img", "", src: path, style: "height: #{size}px; width: #{size}px"
    end

    def display_coin(coin, options={})
      options[:class] ||= "coin"
      content_tag :span, options do
        coin_img(coin) + "&nbsp;".html_safe + content_tag(:span, title: coin) do
          if options[:format]
            sanitize(options[:format] % coin_name(coin))
          else
            sanitize(coin_name(coin))
          end
        end
      end
    end

    private

    def coin_intersection(greater_set, lesser_set)
      if lesser_set.any?
        coins_by_rank(greater_set & lesser_set)
      else
        coins_by_rank(greater_set)
      end
    end

  end
end
