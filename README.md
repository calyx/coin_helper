# Coin Helper

Information, localization, and icons for various cryptocurrencies. Can be used as an Engine for Ruby on Rails.

## Usage

```ruby
class ApplicationController < ActionController::Base
  helper CoinHelper::Engine.helpers
end
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'coin_helper'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install coin_helper
```

## Links

Free APIs with comprehensive data on nearly all coins
https://www.coingecko.com/en/api
https://www.coinlore.com/cryptocurrency-data-api

Free API for all coin icons
https://cryptoicons.org/

Git repo with coin icons
https://github.com/spothq/cryptocurrency-icons

Current transaction fees (BTC only)
https://api.blockchain.info/mempool/fees
https://bitcoinfees.earn.com/api/v1/fees/recommended

Current transaction fees (ETH only)
https://etherscan.io/apis#gastracker

Ruby binding for cryptocompare API
https://github.com/alexanderdavidpan/cryptocompare

List of APIs
https://github.com/coinpride/CryptoList#for-developers

Environmental rating of different coins
https://www.cryptowisser.com/crypto-carbon-footprint
https://www.carbon-ratings.com/

## Development

To update the data in this gem (in this order):

    rake update_carbon_data
    rake update_coin_data
    rake build

The SVG files are manually copied from https://github.com/spothq/cryptocurrency-icons, and then:

    FORCE=true rake render


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

The icons used in this gem are public domain (licensed CC0) and are taken from https://github.com/spothq/cryptocurrency-icons
