$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "coin_helper/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "coin_helper"
  spec.version     = CoinHelper::VERSION
  spec.authors     = ["Elijah Waxwing"]
  spec.email       = ["elijah@calyxinstitute.org"]
  spec.homepage    = "https://0xacab.org/calyx/gems/coin_helper"
  spec.summary     = "Info and images for cryptocurrencies"
  spec.license     = "MIT"
  spec.metadata["allowed_push_host"] = "https://0xacab.org"

  spec.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]
end
