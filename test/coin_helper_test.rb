require 'minitest/autorun'
require_relative '../lib/coin_helper'
require_relative '../app/helpers/coin_helper/cryptocurrency_helper'

class CoinHelperTest < Minitest::Test
  include CoinHelper::CryptocurrencyHelper

  def setup
  end

  def test_coins
    assert_equal ["BTC", "ETH"], coins_by_rank(["ETH", "BTC"])
  end

  def test_green
    assert_equal ["XNO", "ALGO"], green_coins(["ALGO", "XNO"])
    assert_equal ["NANO", "AVAX"], green_coins(["NANO", "AVAX"])
  end

  def test_data
    xno = {"name"=>{"en"=>"Nano"}, "icon"=>"xno", "rank"=>10, "carbon"=>"A", "stable"=>false, "private" => false}
    dot = {"name"=>{"en"=>"Polkadot"}, "icon"=>"dot", "rank"=>10, "carbon"=>"C", "stable"=>false, "private" => false}
    assert_equal xno, coin_data('XNO')
    assert_equal dot, coin_data('DOT')
  end

end
