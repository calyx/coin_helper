require 'minitest/autorun'
require_relative '../lib/coin_helper'
require_relative '../app/helpers/coin_helper/cryptocurrency_helper'
require 'action_view'

class CoinDisplayTest < ActionView::TestCase
  include CoinHelper::CryptocurrencyHelper

  def setup
  end

  def test_display
    expected = '<span class="coin"><img src="/coin_helper/color/22/xno.png" style="height: 22px; width: 22px"></img>&nbsp;<span title="XNO">Nano</span></span>'
    assert_equal expected, display_coin("XNO")
  end
end
