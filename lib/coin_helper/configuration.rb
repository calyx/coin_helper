module CoinHelper
  class Configuration
    # set to `auto_include` to true to auto-include the helper in
    # every controller.
    #
    # Otherwise, you have to put this in the controllers:
    #
    # class PaymentController < Application
    #   helper CoinHelper::CryptocurrencyHelper
    # end
    #
    attr_accessor :auto_include

    def initialize
      @auto_include = false
    end
  end
end